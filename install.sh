#! /bin/bash

docker network create dns

cd nodejs_frontend
docker build -t front .

cd ../nodejs_rest_api
docker build -t rest .

cd ../nodejs_webtoken
docker build -t token .

docker run -d -p 27017:27017 --name mongo --network dns mongo

sleep 10

docker run -d -p 80:80 --network dns --name front front
docker run -d -p 8081:8081 --network dns --name rest rest
docker run -d -p 8082:8082 --network dns --name token token
